package com.softeria.tech.samplerestserver.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class APIController {

    @RequestMapping(path = "/",method = RequestMethod.GET)
    public List<String> index(){
        return new ArrayList<String>(){{
            add("one");
            add("two");
            add("three");
            add("four");
        }};
    }
}
