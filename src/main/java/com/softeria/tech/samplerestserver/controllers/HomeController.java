package com.softeria.tech.samplerestserver.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
    @Value("${spring.application.name}")
    String appName;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String homePage(Model model, Authentication authentication) {
        model.addAttribute("appName", appName);
        model.addAttribute("auth", authentication.getName());
        return "home";
    }

}
