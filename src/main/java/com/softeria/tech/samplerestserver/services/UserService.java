package com.softeria.tech.samplerestserver.services;


import com.softeria.tech.samplerestserver.models.User;

import java.util.List;

public interface UserService {

    User save(User user);
    List<User> findAll();
    void delete(long id);
    User findByUsername(String username);
}
