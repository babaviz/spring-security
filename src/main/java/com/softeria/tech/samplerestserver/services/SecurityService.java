package com.softeria.tech.samplerestserver.services;

public interface SecurityService {
    String findLoggedInUsername();
    void autoLogin(String username, String password);
}
