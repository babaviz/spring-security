package com.softeria.tech.samplerestserver.services.impl;

import com.softeria.tech.samplerestserver.dao.RoleDao;
import com.softeria.tech.samplerestserver.dao.UserDao;
import com.softeria.tech.samplerestserver.models.Role;
import com.softeria.tech.samplerestserver.models.User;
import com.softeria.tech.samplerestserver.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements  UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        User user = userDao.findByUsername(userId);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        Optional<User> byId = userDao.findById(id);
        byId.ifPresent(user -> userDao.delete(user));
    }

    @Override
    public User save(User user) {
        HashSet<Role> roles = new HashSet<>();
        roleDao.findAll().iterator().forEachRemaining(roles::add);
        user.setRoles(roles);
        return userDao.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
