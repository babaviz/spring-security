package com.softeria.tech.samplerestserver.dao;

import com.softeria.tech.samplerestserver.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

}